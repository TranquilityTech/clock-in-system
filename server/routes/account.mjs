import "dotenv/config";
import express from "express";
import jwt from "jsonwebtoken";
import shortid from "shortid";
// models
import User from "#root/models/user";

const router = express.Router();

router.post("/login", async (req, res) => {
  try {
    const user = await User.getByEmail(req.body.email);

    if (!user) throw new Error("User not found");

    const jti = shortid.generate();
    const iat = Math.round(new Date() / 1000);
    const token = jwt.sign(
      {
        iss: "Company",
        iat,
        jti,
        sub: user.email,
        data: {
          user,
        },
      },
      process.env.JWT_SECRET_KEY,
      {
        expiresIn: "15d",
      }
    );

    res.json({
      token,
    });
  } catch (error) {
    res.status(503).end();
  }
});

router.post("/create", async (req, res) => {
  try {
    const { email } = req.body;
    const newUser = { email, createdTime: new Date() };

    if (!email) throw new Error("Email required");

    await User.save(newUser);

    const user = await User.getByEmail(req.body.email);

    const jti = shortid.generate();
    const iat = Math.round(new Date() / 1000);
    const token = jwt.sign(
      {
        iss: "Company",
        iat,
        jti,
        sub: user.email,
        data: {
          user,
        },
      },
      process.env.JWT_SECRET_KEY,
      {
        expiresIn: "15d",
      }
    );

    res.json({
      token,
    });
  } catch (error) {
    res.status(503).end();
  }
});

export default router;
