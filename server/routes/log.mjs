import express from "express";
import moment from "moment-timezone";
// models
import Log from "#root/models/log";

const router = express.Router();

router.get("/", async (req, res) => {
  const { user } = req.app.locals;
  let logs = await Log.getUser(user.email);

  logs = logs.map((log) => ({
    ...log,
    createdTime: moment(log.createdTime).format("LLL"),
  }));

  res.json({
    logs,
  });
});

router.post("/", async (req, res) => {
  try {
    const { office } = req.body;
    const { user } = req.app.locals;
    const now = new Date();
    const hour = moment(now).hours();
    const date = moment(now).format("L");
    let type = "night";

    if (hour > 6 || hour < 10) type = "morning";
    if (hour >= 10 || hour < 14) type = "noon";

    const log = {
      user: user.email,
      office,
      date,
      type,
      createdTime: now,
    };

    await Log.save(log);

    res.json({
      log,
    });
  } catch (error) {
    res.status(503).end();
  }
});

export default router;
