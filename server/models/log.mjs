import { MongoConnection } from "../mongodb.mjs";

class Log {
  static async save(log) {
    try {
      const client = await MongoConnection();
      const bulk = await client.db("Test").collection("Log");
      const { type, date } = log;
      const opts = [
        {
          updateOne: {
            filter: {
              type,
              date,
            },
            update: {
              $setOnInsert: log,
            },
            upsert: true,
          },
        },
      ];

      await bulk.bulkWrite(opts);
    } catch (err) {
      throw new Error(err);
    }
  }

  static async getUser(user) {
    try {
      const client = await MongoConnection();
      const result = await client
        .db("Test")
        .collection("Log")
        .find({
          user,
        })
        .toArray();

      return result;
    } catch (err) {
      throw new Error(err);
    }
  }
}

export default Log;
