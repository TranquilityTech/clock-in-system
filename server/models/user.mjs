import { MongoConnection } from "../mongodb.mjs";

class User {
  static async save(user) {
    try {
      const client = await MongoConnection();
      const bulk = await client.db("Test").collection("User");
      const opts = [
        {
          updateOne: {
            filter: {
              email: user.email,
            },
            update: {
              $setOnInsert: user,
            },
            upsert: true,
          },
        },
      ];

      await bulk.bulkWrite(opts);
    } catch (err) {
      throw new Error(err);
    }
  }

  static async getByEmail(email) {
    try {
      const client = await MongoConnection();
      const result = await client.db("Test").collection("User").findOne({
        email,
      });

      return result;
    } catch (err) {
      throw new Error(err);
    }
  }
}

export default User;
