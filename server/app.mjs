import "dotenv/config";
import express from "express";
import path from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";
import cors from "cors";
import jwt from "jsonwebtoken";

import { fileURLToPath } from "url";
const __dirname = path.dirname(fileURLToPath(import.meta.url));

import { MongoConnect } from "./mongodb.mjs";
MongoConnect();

// models
import User from "#root/models/user";

const app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

import logRouter from "./routes/log.mjs";
import accountRouter from "./routes/account.mjs";

app.use(async (req, res, next) => {
  try {
    if (["/account/login", "/account/create"].includes(req.path)) return next();

    const token = req.header("Authorization")?.replace("Bearer ", "");
    const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);
    const user = await User.getByEmail(decoded.sub);

    if (!user) throw new Error("User not found");

    req.app.locals.user = user;

    next();
  } catch (error) {
    res.status(498).end();
  }
});

app.use("/log", logRouter);
app.use("/account", accountRouter);

export default app;
