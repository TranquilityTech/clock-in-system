// services
import AuthService from "../services/auth";
import "./login.css";

const Login = () => {
  const signupToggle = () => {
    document.querySelector(".message").style.transform = "translateX(100%)";
  };

  const loginToggle = () => {
    document.querySelector(".message").style.transform = "translateX(0)";
  };

  async function handleLoginSubmit(event) {
    event.preventDefault();

    const email = document.querySelector("#login-email").value;
    const res = await fetch("http://localhost:3001/account/login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
      }),
    }).then((res) => res.json());

    if (res.token) AuthService.login(res.token);
    window.location.reload();
  }

  async function handleRegisterSubmit(event) {
    event.preventDefault();

    const email = document.querySelector("#register-email").value;
    const res = await fetch("http://localhost:3001/account/create", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
      }),
    }).then((res) => res.json());

    if (res.token) AuthService.login(res.token);
    window.location.reload();
  }

  return (
    <main>
      <div className="login-container">
        <div className="message signup">
          <div className="btn-wrapper">
            <button className="button" id="signup" onClick={signupToggle}>
              Register
            </button>
            <button className="button" id="login" onClick={loginToggle}>
              Login
            </button>
          </div>
        </div>
        <div className="form form--signup">
          <div className="form--heading">Register</div>
          <form onSubmit={handleRegisterSubmit}>
            <input type="email" placeholder="Email" id="register-email" />
            <button className="button">Submit</button>
          </form>
        </div>
        <div className="form form--login">
          <div className="form--heading">Login</div>
          <form onSubmit={handleLoginSubmit}>
            <input type="text" placeholder="Name" id="login-email" />
            <button className="button">LoSubmitgin</button>
          </form>
        </div>
      </div>
    </main>
  );
};

export default Login;
