import React, { useEffect, useState, useContext } from "react";
import { Theme } from "../contexts/theme";
// services
import AuthService from "../services/auth";
import "./home.css";

const Home = () => {
  const [theme, setTheme] = useContext(Theme);
  const token = AuthService.getToken();
  const user = AuthService.getCurrentUser();
  const [logs, setLogs] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`http://localhost:3001/log`, {
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        });
        const { logs } = await response.json();

        setLogs(logs);
      } catch (error) {
        console.log("error", error);
      }
    };

    fetchData();
  }, [token]);

  const addLog = async () => {
    try {
      const office = document.querySelector("#office").value;
      await fetch(`http://localhost:3001/log`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          office,
        }),
      });

      window.location.reload();
    } catch (error) {
      console.log("error", error);
    }
  };

  const themeToggle = () => {
    const newTheme = theme === "blue" ? "red" : "blue";
    setTheme(newTheme);
    localStorage.setItem("theme", newTheme);
  };

  const logout = () => {
    AuthService.logout();
    window.location.reload();
  };

  return (
    <main className={`home-container ${theme === "blue" ? "blue" : "red"}`}>
      <div className="panels-container">
        <div className="panel left-panel">
          <div className={`wrapper ${theme === "blue" ? "blue" : "red"}`}>
            <h2>{user.email}</h2>
            <select id="office">
              <option value="office1">Office 1</option>
              <option value="office2">Office 2</option>
              <option value="office3">Office 3</option>
            </select>
            <button className="buttom" onClick={addLog}>
              add
            </button>
            <hr />
            <button className="buttom" onClick={themeToggle}>
              theme
            </button>
            <button className="buttom" onClick={logout}>
              logout
            </button>
          </div>
        </div>
        <div className="panel right-panel">
          <table>
            <tr>
              {logs.map((log) => (
                <td key={log._id}>{log.createdTime}</td>
              ))}
            </tr>
            <tr>
              {logs.map((log) => (
                <td key={log._id}>{log.office}</td>
              ))}
            </tr>
          </table>
        </div>
      </div>
    </main>
  );
};

export default Home;
