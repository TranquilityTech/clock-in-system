import jwt_decode from "jwt-decode";

const login = (token) => localStorage.setItem("token", token);
const logout = () => localStorage.removeItem("token");
const getToken = () => localStorage.getItem("token");
const getCurrentUser = () => {
  const token = localStorage.getItem("token");

  if (token) {
    const decode = jwt_decode(token);
    const { user } = decode.data;

    return user;
  }

  return null;
};

const AuthService = {
  login,
  logout,
  getToken,
  getCurrentUser,
};

export default AuthService;
