import { useState, useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { User } from "./contexts/user";
import { Theme } from "./contexts/theme";
// services
import AuthService from "./services/auth";
// pages
import Home from "./pages/home";
import Login from "./pages/login";

function App() {
  const [user, setUser] = useState(undefined);
  const [theme, setTheme] = useState("blue");

  useEffect(() => {
    const userData = AuthService.getCurrentUser();

    if (userData) setUser(userData);
  }, []);

  useEffect(() => {
    const themeData = localStorage.getItem("theme");

    if (themeData) setTheme(themeData);
  }, []);

  return (
    <User.Provider value={[user, setUser]}>
      <Theme.Provider value={[theme, setTheme]}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={user ? <Home /> : <Login />} />
          </Routes>
        </BrowserRouter>
      </Theme.Provider>
    </User.Provider>
  );
}

export default App;
